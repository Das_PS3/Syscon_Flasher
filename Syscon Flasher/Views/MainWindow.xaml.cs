﻿using Microsoft.Win32;
using Syscon_Flasher.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;

namespace Syscon_Flasher
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {        
        #region Fields

        private SerialDevice busPirate;
        private static SerialPort sp;

        private StringBuilder receivedData = new StringBuilder();
        public ObservableCollection<string> Activity { get; set; }

        private Stopwatch sw = new Stopwatch();

        private volatile bool @continue;
        private volatile bool dumpStarted; // DEBUGGING
        private volatile bool subscribed;

        private const int READ_WAIT = 125;
        private const int SETTINGS_WAIT = 150;
        private const int WRITE_WAIT = 350;
        
        #endregion

        #region Constructors

        public MainWindow()
        {
            busPirate = new SerialDevice();
            Refresh_Click(null, null);

            this.DataContext = busPirate;

            Activity = new ObservableCollection<string>();

            subscribed = false;

            InitializeComponent();

            if (File.Exists("debug.log"))
                File.Delete("debug.log");
        }

        #endregion

        #region Command executions

        private void Browse_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Syscon dump file|*.bin";
            ofd.CheckFileExists = false;

            if (ofd.ShowDialog().Value)
                busPirate.BinPath = ofd.FileName;

            if (File.Exists(busPirate.BinPath) && new FileInfo(busPirate.BinPath).Length < 0x8001)
                busPirate.Length = "0x" + new FileInfo(busPirate.BinPath).Length.ToString("X");

            if (!File.Exists(busPirate.BinPath) || busPirate.Length == "0x0")
                busPirate.Length = "0x8000";
        }

        private void Cancel_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            @continue = false;
            subscribed = false;
        }

        private void CheckUpdates_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Assembly thisAssembly = Assembly.GetExecutingAssembly();

            Process.Start("Updater.exe", string.Format("\"{0}\" \"{1}\" \"{2}\"", thisAssembly.GetName().Name, thisAssembly.GetName().Version, Process.GetCurrentProcess().MainModule.FileName));
        }

        private async void FastRead_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            await Task.Run(() =>
            {
                sw.Reset();
                sw.Start();

                @continue = true;

                string length = SetLength(true);

                if (File.Exists(busPirate.BinPath))
                {
                    File.Delete(busPirate.BinPath);
                    Activity.Add(Path.GetFileName(busPirate.BinPath) + " exists. The file will be overwritten.");
                }

                Activity.Add("Began reading");
                subscribed = true;
                App.Current.Dispatcher.Invoke(() => CommandManager.InvalidateRequerySuggested());
                sp.DataReceived += sp_DataReceivedFastRead;

                int tmp = OffsetToIntOffset();
                tmp /= 2;

                string offset = IntOffsetToHex(tmp);
                
                string command = string.Format("[0xA8 " + offset + " r:" + length + "]");
                
                WriteCommand(command, READ_WAIT);
            });
        }

        private void PowerOn_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (busPirate.IsPowerOn)
            {
                sp.WriteLine("W");
                Activity.Add("Bus Pirate Power On");
            }
            else
            {
                sp.WriteLine("w");
                Activity.Add("Bus Pirate Power Off");
            }

            Thread.Sleep(SETTINGS_WAIT);
        }

        private async void Read_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            await Task.Run(() =>
            {
                sw.Reset();
                sw.Start();

                @continue = true;

                string length = SetLength();

                if (File.Exists(busPirate.BinPath))
                {
                    File.Delete(busPirate.BinPath);
                    Activity.Add(Path.GetFileName(busPirate.BinPath) + " exists. The file will be overwritten.");
                }

                Activity.Add("Began reading");
                subscribed = true;
                App.Current.Dispatcher.Invoke(() => CommandManager.InvalidateRequerySuggested());
                sp.DataReceived += sp_DataReceivedRead;

                int lastOffset = 0;
                int leftOver = int.Parse(length) - ((int.Parse(length) / 32) * 32);

                for (int i = 0; i < int.Parse(length) / 32; i++)
                {
                    if (!@continue)
                        break;

                    int tmp = OffsetToIntOffset();

                    tmp /= 2;

                    tmp += i * 16;
                    lastOffset = tmp + 16;

                    string offset = IntOffsetToHex(tmp);

                    string command = string.Format("[0xA8 " + offset + " r:32]");

                    WriteCommand(command, READ_WAIT);
                }

                if (leftOver > 0 && @continue)
                {                    
                    string offset = IntOffsetToHex(lastOffset);

                    string command = string.Format("[0xA8 " + offset + " r:" + leftOver + "]");

                    WriteCommand(command, READ_WAIT);
                }

                Activity.Add("Finished writing data to file");
                subscribed = false;
                App.Current.Dispatcher.Invoke(() => CommandManager.InvalidateRequerySuggested());
                sp.DataReceived -= sp_DataReceivedRead;

                sw.Stop();
                Activity.Add("Time elapsed: " + sw.Elapsed);
            });
        }

        private async void Write_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            await Task.Run(() =>
            {
                byte[] dumpContents;

                if (File.Exists(busPirate.BinPath))
                {
                    dumpContents = File.ReadAllBytes(busPirate.BinPath);

                    if (dumpContents.Length % 2 != 0 || dumpContents.Length == 0)
                    {
                        Activity.Add(Path.GetFileName(busPirate.BinPath) + " is an invalid dump file (missing bytes). Operation aborted.");
                        return;
                    }

                    Activity.Add("Found and loaded the contents of " + Path.GetFileName(busPirate.BinPath));
                }
                else
                {
                    Activity.Add(Path.GetFileName(busPirate.BinPath) + " could not be found. Operation aborted.");
                    return;
                }

                sw.Reset();
                sw.Start();

                @continue = true;

                int length = dumpContents.Length;
                int leftOver = length - ((length / 16) * 16);

                busPirate.TotalBytes = leftOver == 0 ? (uint)(length / 16) * 0x18B :
                                                       (uint)((length / 16) * 0x18B) + 0x5B + (uint)(leftOver * 0x13);

                busPirate.CurrentByte = 0;

                string unlock = "[0xA3 0x00 0x00]";

                WriteCommand(unlock, WRITE_WAIT);

                Activity.Add("Began writing");
                subscribed = true;
                App.Current.Dispatcher.Invoke(() => CommandManager.InvalidateRequerySuggested());
                sp.DataReceived += sp_DataReceivedWrite;

                int lastOffset = 0;
                int lastLocalOffset = 0;

                for (int i = 0; i < length / 16; i++)
                {
                    if (!@continue)
                        break;

                    int tmp = OffsetToIntOffset();

                    tmp /= 2;

                    tmp += i * 8;
                    lastOffset = tmp + 8;

                    string offset = IntOffsetToHex(tmp);

                    string command = string.Format("[0xA4 " + offset + " 0x" + BitConverter.ToString(dumpContents.Skip(i * 16).Take(16).ToArray()).Replace("-", " 0x") + "]");

                    lastLocalOffset = i * 16;

                    WriteCommand(command, WRITE_WAIT);
                }

                if (leftOver > 0 && @continue)
                {
                    string offset = IntOffsetToHex(lastOffset);
                    
                    string command = string.Format("[0xA4 " + offset + " 0x" + BitConverter.ToString(dumpContents.Skip(lastLocalOffset).Take(leftOver).ToArray()).Replace("-", " 0x") + "]");
                    
                    WriteCommand(command, WRITE_WAIT);
                }

                Activity.Add("Finished writing data to syscon");
                subscribed = false;
                App.Current.Dispatcher.Invoke(() => CommandManager.InvalidateRequerySuggested());
                sp.DataReceived -= sp_DataReceivedWrite;

                sw.Stop();
                Activity.Add("Time elapsed: " + sw.Elapsed);
            });
        }

        /// <summary>
        /// Sends the following settings to Bus Pirate:
        /// 
        /// m (mode)
        /// 5 (SPI)
        /// 4 (1MHz) - Maximum supported speed
        /// 2 (Clock polarity: Idle high)
        /// 1 (Output clock edge: Idle to active)
        /// 1 (Input sample phase: Middle *default)
        /// 2 (CS: /CS)
        /// 2 (Select output type: Normal (H=3.3V, L=GND))
        /// </summary>
        /// <param name="sender">Unused</param>
        /// <param name="e">Unused</param>
        private void SendSettings_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (sp != null && sp.IsOpen)
                ResetConnection();

            sp = new SerialPort(busPirate.SelectedPort, 115200, Parity.None, 8, StopBits.One);
            sp.ReadTimeout = busPirate.ReadTimeout;
            sp.WriteTimeout = busPirate.WriteTimeout;
            sp.Handshake = Handshake.None;

            sp.Open();

            Activity.Add("Successfully connected to Bus Pirate on " + busPirate.SelectedPort);
            Activity.Add("Sending settings...");

            string[] settings = new string[] { "m", "5", "4", "2", "1", "1", "2", "2" };
            
            foreach (var command in settings)
                WriteCommand(command, SETTINGS_WAIT);
            
            Activity.Add("Settings sent");
        }

        #endregion

        #region Conditional checks

        private void Browse_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = busPirate.IsPowerOn && !subscribed;
        }

        private void Cancel_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = subscribed;
        }

        private void CheckUpdates_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = File.Exists("Updater.exe");
        }

        private bool IsValid(DependencyObject obj)
        {
            // The dependency object is valid if it has no errors and all of its children (that are dependency objects) are error-free.
            return !Validation.GetHasError(obj) && LogicalTreeHelper.GetChildren(obj)
                                                                    .OfType<DependencyObject>()
                                                                    .All(IsValid);
        }

        private void PowerOn_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = sp != null && !subscribed && sp.IsOpen ? true : false;
        }

        private void ReadWrite_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = busPirate.IsPowerOn && !string.IsNullOrWhiteSpace(busPirate.BinPath) && IsValid(sender as DependencyObject) && !subscribed;
        }

        private void SendSettings_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = !string.IsNullOrWhiteSpace(busPirate.SelectedPort) && !subscribed;
        }

        #endregion

        #region Event handlers

        private void sp_DataReceivedFastRead(object sender, SerialDataReceivedEventArgs e)
        {
            string data = sp.ReadExisting();

            busPirate.CurrentByte += (uint)data.Length;

            if (Activity.Last().Contains("Receiving data:"))
                Activity.RemoveAt(Activity.Count - 1);

            Activity.Add(string.Format("Receiving data: {0}/{1}", busPirate.CurrentByte, busPirate.TotalBytes));

            receivedData.Append(data);
            
            if (data.Contains("/"))
                dumpStarted = true;

            if (data.Contains(">") && dumpStarted)
            {
                WriteSysconToFile();

                Activity.Add("Finished writing data to file");
                subscribed = false;
                dumpStarted = false;
                App.Current.Dispatcher.Invoke(() => CommandManager.InvalidateRequerySuggested());
                sp.DataReceived -= sp_DataReceivedFastRead;

                sw.Stop();
                Activity.Add("Time elapsed: " + sw.Elapsed);
            }
        }

        private void sp_DataReceivedRead(object sender, SerialDataReceivedEventArgs e)
        {
            string data = sp.ReadExisting();

            busPirate.CurrentByte += (uint)data.Length;

            if (Activity.Last().Contains("Receiving data:"))
                Activity.RemoveAt(Activity.Count - 1);

            Activity.Add(string.Format("Receiving data: {0}/{1}", busPirate.CurrentByte, busPirate.TotalBytes));
                        
            receivedData.Append(data);

            if (data.Contains(">"))
                WriteSysconToFile();
        }

        private void sp_DataReceivedWrite(object sender, SerialDataReceivedEventArgs e)
        {
            string data = sp.ReadExisting();

            busPirate.CurrentByte += (uint)data.Length;

            if (Activity.Last().Contains("Writing data:"))
                Activity.RemoveAt(Activity.Count - 1);

            Activity.Add(string.Format("Writing data: {0}/{1}", busPirate.CurrentByte, busPirate.TotalBytes));

            receivedData.Append(data);

            if (data.Contains(">"))
                receivedData.Clear();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            (sender as TextBox).ScrollToEnd();
        }

        #endregion

        #region Logic

        private string IntOffsetToHex(int tmp)
        {
            byte[] tmpBA = BitConverter.GetBytes(tmp).Take(2).ToArray();

            if (BitConverter.IsLittleEndian)
                Array.Reverse(tmpBA);

            string offset = "0x" + BitConverter.ToString(tmpBA).Replace("-", " 0x");

            return offset;
        }

        private int OffsetToIntOffset()
        {
            bool isOffsetDigitsOnly = SerialDevice.digitsOnlyRegex.IsMatch(busPirate.Offset);

            int tmp;

            if (isOffsetDigitsOnly)
                tmp = Convert.ToInt32(busPirate.Offset);
            else
                tmp = Convert.ToInt32(busPirate.Offset.Substring(2), 16);

            return tmp;
        }

        private void ResetConnection()
        {
            sp.DiscardInBuffer();
            Thread.Sleep(SETTINGS_WAIT);
            sp.DiscardOutBuffer();
            Thread.Sleep(SETTINGS_WAIT);
            sp.Close();
            Thread.Sleep(SETTINGS_WAIT * 2);

            busPirate.CurrentByte = 0;
            busPirate.TotalBytes = 0;
        }

        private string SetLength(bool isFastRead = false)
        {
            bool isLengthDigitsOnly = SerialDevice.digitsOnlyRegex.IsMatch(busPirate.Length);

            string length;

            if (isLengthDigitsOnly)
                length = busPirate.Length;
            else
            {
                int tmp = Convert.ToInt32(busPirate.Length.Substring(2), 16);

                length = tmp.ToString();
            }

            int leftOver = int.Parse(length) - ((int.Parse(length) / 32) * 32);

            if (!isFastRead)
                busPirate.TotalBytes = leftOver == 0 ? (uint.Parse(length) / 32) * 0x108 :
                                                   ((uint.Parse(length) / 32) * 0x108) + 0x58 + (uint)length.Length + ((uint)leftOver * 5);
            else
                busPirate.TotalBytes = (uint.Parse(length) * 5) + 0x66 + (uint)length.Length;

            busPirate.CurrentByte = 0;

            return length;
        }

        private void WriteCommand(string command, int sleepTime)
        {
            sp.DiscardInBuffer();
            Thread.Sleep(sleepTime);
            
            sp.WriteLine(command);
            Thread.Sleep(sleepTime);
        }

        #endregion

        #region Private methods

        private void Refresh_Click(object sender, RoutedEventArgs e)
        {
            SerialPort serialPort = new SerialPort();

            List<string> availablePorts = new List<string>();

            foreach (var portName in SerialPort.GetPortNames())
                availablePorts.Add(portName);

            busPirate.AvailablePorts = availablePorts.ToArray();
        }

        private void WriteSysconToFile()
        {
            string dumpRaw = receivedData.ToString();

            int startIndex = dumpRaw.IndexOf("READ: ") + 6;
            int endIndex = dumpRaw.IndexOf("/CS DISABLED") - 2;

            string cleanDump = dumpRaw.Substring(startIndex, endIndex - startIndex).Replace("0x", "").Replace(" ", "");
            
            int tmp = OffsetToIntOffset();

            string offset = IntOffsetToHex(tmp / 2);

            if (dumpRaw.Contains(offset) && tmp % 2 != 0)
                cleanDump = cleanDump.Substring(2);

            byte[] dump = Enumerable.Range(0, cleanDump.Length)
                                    .Where(x => x % 2 == 0)
                                    .Select(x => Convert.ToByte(cleanDump.Substring(x, 2), 16))
                                    .ToArray();
            
            using (FileStream fs = new FileStream(busPirate.BinPath, FileMode.Append, FileAccess.Write, FileShare.Read))
            using (BinaryWriter bw = new BinaryWriter(fs))
                bw.Write(dump);

            receivedData.Clear();
        }

        #endregion
    }
}
