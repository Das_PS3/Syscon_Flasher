﻿using Syscon_Flasher.Views;
using System;
using System.Windows;

namespace Syscon_Flasher
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region Exception handlers

        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;

            new ExceptionViewer("An unrecoverable error has occurred", e.Exception, Application.Current.MainWindow).ShowDialog();

            Application.Current.Shutdown(1);
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            new ExceptionViewer("An unrecoverable error has occurred", (Exception)e.ExceptionObject, Application.Current.MainWindow).ShowDialog();

            Application.Current.Shutdown(1);
        }

        #endregion

        #region Entry point

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            MainWindow mainWindow = new MainWindow();
            Application.Current.MainWindow = mainWindow;

            mainWindow.Show();
        }

        #endregion
    }
}
