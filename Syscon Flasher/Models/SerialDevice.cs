﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;

namespace Syscon_Flasher.Models
{
    public class SerialDevice : INotifyPropertyChanged, IDataErrorInfo
    {
        #region Fields

        internal static readonly Regex digitsOnlyRegex = new Regex(@"^[\d]*$", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        internal static readonly Regex hexRegex = new Regex(@"\A\b0x[\da-f]+\b\Z", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        private string[] availablePorts;

        private string selectedPort, binPath, offset, length;

        private int readTimeout, writeTimeout;

        private uint currentByte, totalBytes;

        private bool isPowerOn;

        #endregion

        #region Constructors

        public SerialDevice()
        {
            this.length = "0x8000";
            this.offset = "0";
            this.readTimeout = -1;
            this.writeTimeout = -1;
        }

        #endregion

        #region Properties

        public string[] AvailablePorts
        {
            get { return availablePorts; }
            set
            {
                if (availablePorts != null && Enumerable.SequenceEqual(availablePorts, value)) return;
                availablePorts = value;
                NotifyPropertyChanged("AvailablePorts");
            }
        }

        public string SelectedPort 
        {
            get { return selectedPort; }
            set
            {
                if (selectedPort == value) return;
                selectedPort = value;
                NotifyPropertyChanged("SelectedPort");
            }
        }

        public int ReadTimeout
        {
            get { return readTimeout; }
            set
            {
                if (readTimeout == value) return;
                readTimeout = value;
                NotifyPropertyChanged("ReadTimeout");
            }
        }

        public string BinPath
        {
            get { return binPath; }
            set
            {
                if (binPath == value) return;
                binPath = value;
                NotifyPropertyChanged("BinPath");
            }
        }

        public int WriteTimeout
        {
            get { return writeTimeout; }
            set
            {
                if (writeTimeout == value) return;
                writeTimeout = value;
                NotifyPropertyChanged("WriteTimeout");
            }
        }

        public bool IsPowerOn
        {
            get { return isPowerOn; }
            set
            {
                if (isPowerOn == value) return;
                isPowerOn = value;
                NotifyPropertyChanged("IsPowerOn");
            }
        }

        public string Offset
        {
            get { return offset; }
            set
            {
                if (offset == value) return;

                int intOffset = OffsetToIntOffset(value);
                
                int intLength;
                if (Length != null)
                    intLength = LengthToIntLength(Length);
                else
                    intLength = 0;

                if (intOffset > 0x7FFE)
                {
                    offset = "0x7FFE";
                    intOffset = 0x7FFE;
                }

                if (intOffset + intLength > 0x8000)
                    offset = "0x" + (0x8000 - intLength).ToString("X");

                if (intOffset < 0x7FFE && intOffset + intLength <= 0x8000)
                    offset = value;

                NotifyPropertyChanged("Offset");
            }
        }

        public string Length
        {
            get { return length; }
            set
            {
                if (length == value) return;

                int intOffset = OffsetToIntOffset(Offset);
                int intLength = LengthToIntLength(value);

                if (intLength > 0x8000)
                    length = "0x8000";

                if (intOffset + intLength > 0x8000)
                    length = "0x" + (0x8000 - intOffset).ToString("X");

                if (intLength <= 0x8000 && intOffset + intLength <= 0x8000)
                    length = value;

                NotifyPropertyChanged("Length");
            }
        }

        public uint CurrentByte
        {
            get { return currentByte; }
            set
            {
                if (currentByte == value) return;
                currentByte = value;
                NotifyPropertyChanged("CurrentByte");
                NotifyPropertyChanged("Progress");
                NotifyPropertyChanged("ProgressInt");
            }
        }

        public uint TotalBytes
        {
            get { return totalBytes; }
            set
            {
                if (totalBytes == value) return;
                totalBytes = value;
                NotifyPropertyChanged("TotalBytes");
            }
        }

        public double Progress 
        { 
            get 
            { 
                double percentage = (double)((double)(currentByte * 100) / (double)totalBytes);

                if (double.IsNaN(percentage))
                    return 0;
                else
                    return percentage;
            }
        }

        public int ProgressInt
        {
            get { return (int) RoundUp((uint)Progress, 1); }
        }

        #endregion

        #region Private methods

        private int LengthToIntLength(string value)
        {
            if (value.Length == 0)
                return 1;

            bool isLengthDigitsOnly = digitsOnlyRegex.IsMatch(value);
            bool isHexString = hexRegex.IsMatch(value);

            if (isLengthDigitsOnly)
                return Convert.ToInt32(value);
            else if (isHexString)
                return Convert.ToInt32(value.Substring(2), 16);
            else
                return 1;
        }

        private int OffsetToIntOffset(string value)
        {
            if (value.Length == 0)
                return 0;

            bool isOffsetDigitsOnly = digitsOnlyRegex.IsMatch(value);
            bool isHexString = hexRegex.IsMatch(value);

            if (isOffsetDigitsOnly)
                return Convert.ToInt32(value);
            else if (isHexString)
                return Convert.ToInt32(value.Substring(2), 16);
            else
                return 0;
        }

        private uint RoundUp(uint numToRound, uint multiple)
        {
            if (multiple == 0)
                return numToRound;

            uint remainder = numToRound % multiple;

            if (remainder == 0)
                return numToRound;

            return numToRound + multiple - remainder;
        }

        #endregion

        #region INotifyPropertyChanged Implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region IDataErrorInfo Implementation

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string source]
        {
            get
            {
                if (source == "Offset")
                {
                    if (string.IsNullOrWhiteSpace(this.Offset))
                        return "Enter a decimal or hexadecimal value";

                    bool isDigitsOnly = digitsOnlyRegex.IsMatch(Offset);
                    bool isHexString = hexRegex.IsMatch(Offset);

                    if (!isDigitsOnly && !isHexString)
                        return "Invalid value. Enter a valid, positive decimal or hexadecimal value";

                    int offsetValue = 0;

                    if (isDigitsOnly)
                        offsetValue = int.Parse(Offset);
                    else if (isHexString)
                        offsetValue = Convert.ToInt32(Offset.Substring(2), 16);

                    if (offsetValue > 0x7FFE)
                        return "Invalid value. The value must not exceed 32766 or 0x7FFE";

                    if (offsetValue % 0x10 != 0)
                        return "Invalid value. The value must be a multiple of 16 or 0x10.";
                }

                if (source == "Length")
                {
                    if (string.IsNullOrWhiteSpace(Length))
                        return "Enter a decimal or hexadecimal value";

                    bool isDigitsOnly = digitsOnlyRegex.IsMatch(Length);
                    bool isHexString = hexRegex.IsMatch(Length);

                    if (!isDigitsOnly && !isHexString)
                        return "Invalid value. Enter a valid decimal or hexadecimal value";

                    int lengthValue = 0;

                    if (isDigitsOnly)
                        lengthValue = int.Parse(Length);
                    else if (isHexString)
                        lengthValue = Convert.ToInt32(Length.Substring(2), 16);

                    if (lengthValue > 0x8000)
                        return "Invalid value. The value must not exceed 32768 or 0x8000";

                    if (lengthValue % 2 != 0)
                        return "Invalid value. The value must be a multiple of 2.";

                    if (lengthValue == 0)
                        return "Length cannot be 0!";
                }

                return null;
            }
        }

        #endregion
    }
}
