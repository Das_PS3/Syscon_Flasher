﻿using System;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Data;

namespace Syscon_Flasher.Converters
{
    class ObservableCollectionStrToString : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            ObservableCollection<string> entries = values[0] as ObservableCollection<string>;

            if (entries != null && entries.Count > 0)
            {
                StringBuilder sb = new StringBuilder();

                for (int i = 0; i < entries.Count; i++)
                    sb.AppendLine(entries[i]);

                return sb.ToString();
            }
            else
                return String.Empty;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
